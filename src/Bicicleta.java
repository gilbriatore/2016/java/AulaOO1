public class Bicicleta {	
	
	private int rpm;
	private int qtdeDeMarchas;
	private int marchaAtual;
	private final int LIMITE_RPM = 83;
	private String nome;
	
	public String getNome(){
		return nome;
	}
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public boolean isEmAltaVelocidade(){
		boolean isEmAlta = false;
		if(rpm > LIMITE_RPM){
			isEmAlta = true;
		}
		return isEmAlta;
	}
	
	public int getMarchaAtual(){
		return marchaAtual;
	}
	
	public void setMarchaAtual(int marchaAtual){
		this.marchaAtual = marchaAtual;
	}
	
	public int getQtdeDeMarchas(){
		return qtdeDeMarchas;
	}
	
	public void setQtdeDeMarchas(int qtdeDeMarchas){
		this.qtdeDeMarchas = qtdeDeMarchas;
	}
	
	public int getRpm(){
		return rpm;
	}
	
	public void setRpm(int rpm){
		this.rpm = rpm;
	}
}