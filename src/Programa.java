import java.util.ArrayList;
import java.util.List;

public class Programa {

	public static void main(String[] args) {

		// Cria��o de uma lista din�mica.
		List<Bicicleta> lista = new ArrayList<>();

		// Cria a primeira bicicleta e atribui
		// seus valores.
		Bicicleta bike1 = new Bicicleta();
		bike1.setNome("BIKE 1");
		bike1.setMarchaAtual(5);
		bike1.setRpm(90);
		bike1.setQtdeDeMarchas(10);

		// Cria a segunda e atribui os valores.
		Bicicleta bike2 = new Bicicleta();
		bike2.setNome("BIKE 2");
		bike2.setMarchaAtual(1);
		bike2.setRpm(0);
		bike2.setQtdeDeMarchas(18);

		// Adicionando as bikes na lista.
		lista.add(bike1);
		lista.add(bike2);

		// Verificar se a bike1 est� em movimento.
		if (bike1.getRpm() > 0) {
			System.out.println("A bike 1 est� em movimento!");
		} else {
			System.out.println("A bike1 est� parada!");
		}

		// Verificar se a bike2 est� em movimento.
		if (bike2.getRpm() > 0) {
			System.out.println("A bike 2 est� em movimento!");
		} else {
			System.out.println("A bike2 est� parada!");
		}

		// Verificar se est� em alta velocidade.
		if (bike1.isEmAltaVelocidade()) {
			System.out.println("A bike 1 est� em alta velocidade!");
		} else {
			System.out.println("A bike 1 est� em baixa velocidade!");
		}

		// Verificar se est� em alta velocidade.
		if (bike2.isEmAltaVelocidade()) {
			System.out.println("A bike 2 est� em alta velocidade!");
		} else {
			System.out.println("A bike 2 est� em baixa velocidade!");
		}

		// Imprimir os dados das bikes
		for (Bicicleta bike : lista) {
			System.out.println("-------------------------");
			System.out.println("Nome: " + bike.getNome());
			System.out.println("Marcha atual: " + bike.getMarchaAtual());
			System.out.println("RPM: " + bike.getRpm());
			System.out.println("Qtde de marchas: " + bike.getQtdeDeMarchas());
		}
	}
}